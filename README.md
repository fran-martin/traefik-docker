# Traefik v2.2 en Docker

## Ejecutar el contenedor de Traefik

### Establecer los permisos necesarios para el archivo acme.json
Traefik solo podrá usar este archivo si el usuario root dentro del contenedor tiene acceso exclusivo de lectura y escritura a este.

```
$ chmod 600 acme.json
```
### Registrar contenedores con Traefik

Para hacer esto deberemos indicar a nuesto contenedor las `labels` correspondientes como se muestra en el servicio `nginx` de ejemplo.